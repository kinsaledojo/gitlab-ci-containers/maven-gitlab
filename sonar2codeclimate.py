import json

# Opening JSON file
f = open('sonar.json')

# returns JSON object as
# a dictionary
data = json.load(f)

issuesArray = []

# Iterating through the json
# list
for issue in data['issues']:
    if 'resolution' in issue:
       print(issue['resolution'])
    else:
        if 'textRange' in issue:
            location = {
                "path": issue['component'].split(":")[1] + ':' + str(issue['textRange']['startLine']),
                "positions": {
                    "begin": {
                        "line": issue['textRange']['startLine'],
                        "column": issue['textRange']['startOffset']
                    },
                    "end": {
                        "line": issue['textRange']['endLine'],
                        "column": issue['textRange']['endOffset']
                    }
                }
            }
        elif 'line' in issue:
            location = {
                "path": issue['component'].split(":")[1] + ':' + issue['textRange']['startLine'],
                "lines": {
                    "begin": issue['textRange']['startLine'],
                    "end": issue['textRange']['endLine']
                  }
            }
        else:
            location = {
                "path": issue['component'].split(":")[1]
            }
        iss = {
            "type": "issue",
            "check_name": issue['rule'],
            "description": issue['message'],
            "categories": issue['type'],
            "location": location,
            "remediation_points": 50000,
            "severity": issue['severity'],
            "fingerprint": issue['key']
        }
        print(iss)
        issuesArray.append(iss)
json_object = json.dumps(issuesArray, indent=2)
with open("gl-code-quality-report.json", "w") as outfile:
    outfile.write(json_object)

# Closing file
f.close()
